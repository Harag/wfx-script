(in-package :wfx-script)

(defun transform-variable-name (variable)
  (ppcre:regex-replace-all " +" variable "-"))

(defun transform-variable-names (variables)
  (map 'list #'transform-variable-name variables))

;; (defmethod render-row-editor ((grid import-script-grid) row)
;;   (let* ((form (make-widget 'form :name "import-script-fields-form"
;;                                   :form-id "import-script-fields-edit-form"
;;                                   :grid-size 12
;;                                   :header "Scripts"))
;;          (form-section (make-widget 'form-section
;;                                     :name "form-section"))
;;          (import (current-import grid))
;;          (fields (find-allsorts-for-select
;;                   (format nil "Import Type - ~a"
;;                           (import-type import)))))
;;     (render form
;;             :grid grid
;;             :content
;;             (with-html-string
;;               (render
;;                form-section
;;                :label "Column"
;;                :input
;;                (with-html-string
;;                  (render-edit-field
;;                   "field"
;;                   (field row)
;;                   :type :select
;;                   :data fields)))
;;               (render
;;                form-section
;;                :label "Available columns"
;;                :input
;;                (format nil "~{~(~a~)~^, ~}"
;;                        (transform-variable-names (csv-fields import))))

;;               (render
;;                form-section
;;                :label "Available fucntions"
;;                :input
;;                "if, not, and, or, equal, equalp, =, /=, +, *, /, floor, ceiling, mod, rem, subseq, replace-all, split-string, format, parse-number")
;;               (render
;;                form-section
;;                :label "Script"
;;                :input
;;                (with-html-string
;;                  (render-edit-field
;;                   "script"
;;                   (cond ((post-parameter "script"))
;;                         ((or (raw-script row)
;;                              (script row))
;;                          (let ((*package* #.*package*))
;;                            (or (raw-script row) 
;;                                (write-to-string (script row)
;;                                                 :case :downcase))))
;;                         (t
;;                          "")))))))))

