(defpackage :wfx-script
  (:use :cl :dx-utils)
  (:export
   #:transform-variable-name
   #:transform-variable-names
   #:compile-script
   #:*script-operators*
   #:variables
   #:*defun-enabled*
   #:variable-value))
