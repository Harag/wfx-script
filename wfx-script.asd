(defsystem wfx-script
  :depends-on (dx-utils)
  :serial t
  :components
  ((:file "package")
   (:file "compiler")
   (:file "script")))
